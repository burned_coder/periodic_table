'use strict'

$(document).ready(function(){ 
    function busqueda(elemPerio, elemento){        
        let pos = -1;

        let i = 0;
        
        while(pos == -1 && i<elemento.length){
            if(elemento[i]==elemPerio){
                find=true;
                pos=i;
            }
            i++;
        }

        return pos;
    }

    let temp = 5;
    let titulo = ['T', 'a','b', 'l', 'a', ' ', 'P', 'e', 'r', 'i', 'o', 'd', 'i', 'c', 'a'];
    let tit = $('#titulo');
    const BAR = '_';
    let opcion = false;

    let tiempo = setInterval(function(){        
        let frase = ''; 

        if(!opcion){
            
            temp++;

            if(temp == titulo.length){
                opcion = true;
            }
            
                  
        }else{
            
            if(temp == 6){
                opcion = false;  
            }

            temp--;
        }     

        for(let i = 0; i < temp;i++){
            frase+=titulo[i];
        }

        tit.text(frase+BAR);

    }, 800);

    let divPar = $('#descripcion');
    let divElem;

    let elemento = ['H', 'He', 'Li', 'Be', 'B', 'C', 'N', 'O', 'F', 'Ne', 'Na', 'Mg', 'Al', 'Si', 'P', 'S', 'Cl', 'Ar', 'K', 'Ca', 'Sc', 'Ti', 'V', 'Cr', 'Mn', 'Fe', 'Co', 'Ni', 'Cu', 'Zn', 'Ga', 'Ge', 'As', 'Se', 'Br', 'Kr',
                    'Rb', 'Sr', 'Y', 'Zr', 'Nb', 'Mo', 'Tc', 'Ru', 'Rh', 'Pd', 'Ag', 'Cd', 'In', 'Sn', 'Sb', 'Te', 'I', 'Xe', 'Cs', 'Ba', 'La', 'Hf', 'Ta', 'W', 'Re', 'Os', 'Ir', 'Pt', 'Au', 'Hg', 'Tl', 'Pb', 'Bi', 'Po', 'At',
                    'Rn', 'Fr', 'Ra', 'Ac', 'Rf', 'Db', 'Sg', 'Bh', 'Hs', 'Mt', 'Ds', 'Rg', 'Cn', 'Nh', 'Fl', 'Mc', 'Lv', 'Ts', 'Og', 'Ce', 'Pr', 'Nd', 'Pm', 'Sm', 'Eu', 'Gd', 'Tb', 'Dy', 'Ho', 'Er', 'Tm', 'Yb', 'Lu', 'Th', 
                    'Pa', 'U', 'Np', 'Pu', 'Am', 'Cm', 'Bk', 'Cf', 'Es', 'Fm', 'Md', 'No', 'Lr'];
    let densidadElem = [0.000082, 0.000164, 0.534, 1.85, 2.34, 2.2, 0.001145, 0.001308, 0.001553, 0.000825, 0.97, 1.74, 2.70, 2.3296, 1.823, 2.07, 0.002898, 0.001633, 0.89, 1.54, 2.99, 4.506, 6, 7.15, 7.3, 7.87, 8.86, 8.9, 
                        8.96, 7.134, 5.91, 5.3234, 5.75, 4.809, 3.1028, 0.003425, 1.53, 2.64, 4.47, 6.52, 8.57, 10.2, 11, 12.1, 12.4, 12, 10.5, 8.69, 7.31, 7.287, 6.68, 6.232, 4.933, 0.005366, 1.873, 3.62, 6.15, 13.3, 16.4, 
                        19.3, 20.8, 22.5872, 22.5625, 21.5, 19.3, 13.5336, 11.8, 11.3, 9.79, 9.20, 'Desconocida', 0.009074, 'Desconocida', 5, 10, 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 
                        'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 6.77, 6.77, 7.01, 7.26, 7.52, 5.24, 7.90, 8.23, 
                        8.55, 8.80, 9.07, 9.32, 6.90, 9.84, 11.7, 15.4, 19.1, 20.2, 19.7, 12, 13.51, 14.78, 15.1, 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida', 'Desconocida'];
    let nombreElem = ['Hidrógeno', 'Helio', 'Litio', 'Berilio', 'Boro', 'Carbono', 'Nitrógeno', 'Oxígeno', 'Fluor', 'Neon', 'Sodio', 'Magnesio', 'Aluminio', 'Silicio', 'Fósforo', 'Azufre', 'Cloro', 'Argón',
                      'Potasio', 'Calcio', 'Escandio', 'Titanio', 'Vanadio', 'Cromo', 'Manganeso', 'Hierro', 'Cobalto', 'Niquel', 'Cobre', 'Zinc', 'Galio', 'Germanio', 'Arsenico', 'Selenio', 'Bromo', 'Kriptón', 
                      'Rubidio', 'Estroncio', 'Itrio', 'Circonio', 'Niobio', 'Molibdeno', 'Tecnecio', 'Rutenio', 'Rodio', 'Paladio', 'Plata', 'Cadmio', 'Indio', 'Estaño', 'Antimonio', 'Telurio', 'Yodo', 
                      'Xenón', 'Cesio', 'Bario', 'Lantano', 'Hafnio', 'Tántalo', 'Wolframio', 'Renio', 'Osmio', 'Iridio', 'Platino', 'Oro', 'Mercurio', 'Talio', 'Plomo', 'Bismuto', 'Polonio', 'Ástato', 
                      'Radón', 'Francio', 'Radio', 'Actinio', 'Rutherfordio', 'Dubnio', 'Seaborgio', 'Bohrio', 'Hasio', 'Meitnerio', 'Darmstatio', 'Roentgenio', 'Copernicio', 'Nihonio', 'Flerovio', 
                      'Moscovio', 'Livermorio', 'Teneso', 'Oganesón', 'Lantano', 'Cerio', 'Praseodimio', 'Neodimio', 'Prometio', 'Samario', 'Europio', 'Gadolinio', 'Terbio', 'Disprosio', 'Holmio', 'Erbio', 
                      'Tulio', 'Iterbio', 'Lutecio', 'Torio', 'Protactinio', 'Uranio', 'Neptunio', 'Plutonio', 'Americio', 'Curio', 'Berkelio', 'Californio', 'Einstenio', 'Fermio', 'Mendelevio', 'Nobelio', 
                      'Lawrencio'];

    
    
    let elem = $('[class]');
    
    elem.mouseover(function(){
        let that= $(this);
        that.css('borderColor','red').css('transition','0.5s');
        
        let elemPerio = that.find('h3').text();

        let pos = busqueda(elemPerio,elemento);
        
        //Quitamos el elemento <p> bajo el <h1>(Tabla periodica)
        divPar.find('p').css('display','none');

        //Creamos un <div> donde vamos a darle un grid y una serie de estilos
        //divElem = document.createElement('div');
        divElem = document.createElement('div');
        //https://static.vecteezy.com/system/resources/previews/002/479/627/non_2x/physics-and-sciense-seamless-pattern-with-sketch-elements-hand-drawn-doodles-background-illustration-vector.jpg
        //divElem.style.backgroundImage = "url('https://img.blogs.es/anexom/wp-content/uploads/2021/10/que-es-un-atomo.jpg')";
        divElem.style.backgroundImage = "url('https://img.freepik.com/vector-gratis/fisica-patron-plano-inconsutil-textura-vector_51635-1675.jpg?w=2000')";
        //divElem.style.backgroundPosition = 'bottom';
        divElem.style.backgroundRepeat = 'repeat';
        divElem.style.backgroundSize = '60rem';
        divElem.style.width = '100%'
        divElem.style.border = '2px solid goldenrod';
        divElem.style.borderRadius = '5px';
        divElem.style.backgroundColor='#FFFFFFBF';
        divElem.style.display ='grid';
        divElem.style.gridTemplateRows='auto';
        divElem.style.gridTemplateColumns = 'repeat(3,1fr)';        
        
        //Creamos un elemento <img> al que se le da un atributo y un estilo
        //let imagen = document.createElement('img');
        //imagen.setAttribute('src','https://img.blogs.es/anexom/wp-content/uploads/2021/10/que-es-un-atomo.jpg');
        //imagen.style.width = '100%';
        //imagen.style.height = '100%';
        //imagen.style.objectFit = 'cover';

        //let divImg = document.createElement('div');          
        //divImg.append(imagen); 
        
        //Creamos un div vacío que no va a tener nada en principio
        //let divVacio = document.createElement('div');
        //let lineaVertical = document.createElement('hr');
        //lineaVertical.style.height = '100%';
        //divVacio.append(lineaVertical);

        //Primera opcion
        /*
        let datosCompDiv = document.createElement('div');
        let primerDatoCompH4 = document.createElement('h4');
        let segundoDatoCompH4 = document.createElement('h4');
        primerDatoCompH4.innerHTML = " ==> Nombre: "+ nombreElem[pos];
        segundoDatoCompH4.innerHTML = " ==> Densidad: "+ densidadElem[pos];
        primerDatoCompH4.style.borderBottom = '2px solid black';
        segundoDatoCompH4.style.borderBottom = '2px solid black';
        primerDatoCompH4.style.width = '57%'
        segundoDatoCompH4.style.width = '57%';
        datosCompDiv.append(primerDatoCompH4);
        datosCompDiv.append(segundoDatoCompH4);
        datosCompDiv.style.textAlign='center';
        */

        //Creamos un elemento <div> y otro <table>
        let datosCompDiv = document.createElement('div');
        datosCompDiv.style.display = 'flex';
        datosCompDiv.style. justifyContent = 'center';
        let tabla = document.createElement('table');

        //Creamos una matiz con los valores deseados
        let nombyDens = [[],['Nombre', nombreElem[pos]], ['Densidad', densidadElem[pos]]];

        //Doble bucle for con el que rellenamos la tabla con lso datos
        for (let i = 0; i < 3; i++) {
            let tr = tabla.insertRow();
            for (let j = 0; j < 2; j++) { 
                let td = tr.insertCell();             
                if(i == 0 && j == 0){                                   
                    td.appendChild(document.createTextNode('Datos de Elemento'));
                    td.style.color = 'white';
                    td.style.border = '2px solid goldenrod';
                    td.style.padding = '.5rem 0px';
                    td.style.backgroundColor = 'black';
                    td.style.textAlign = 'center'; 
                    td.style.fontWeight = 'bolder';
                    td.setAttribute('colspan','2'); 
                }else if(i == 1 || i == 2){                    
                    td.appendChild(document.createTextNode(nombyDens[i][j]));
                    td.style.border = '2px solid goldenrod';
                    td.style.padding = '.5rem .5rem'; 
                    td.style.textAlign = 'center';
                    td.style.fontWeight = 'bolder';
                    if(i == 1){
                        td.style.backgroundColor = '#453c8dCC';
                        td.style.color = 'goldenrod';
                    }else if (i == 2){
                        td.style.backgroundColor = '#114409CC';
                        td.style.color = 'goldenrod';
                    }
                }            
                             
            }
        }
        
        //Estilos para la tabla
        tabla.style.borderCollapse = 'collapse'; 
        tabla.style.minWidth = '190px'

        
        //Metemos la tabla en el div creado más arriba
        datosCompDiv.append(tabla);
        datosCompDiv.style.gridColumn = '2';
        datosCompDiv.style.padding = '2.1rem';
        datosCompDiv.style.width = '100%';  

        //Añadimos los elementos creados
        //divElem.append(divImg);
        //divElem.append(divVacio);
        divElem.append(datosCompDiv);

        divPar.append(divElem);  
    });

    elem.mouseout(function(){
        $(this).css('borderColor','goldenrod');
        divElem.remove();
        divPar.find('p').css('display','block');        
    });
});